// Graphics variables
var container;
var camera, composer, scene, renderer;
var textureLoader;
var clock = new THREE.Clock();

var mouseCoords = new THREE.Vector2();
var raycaster = new THREE.Raycaster();
var ballMaterial = new THREE.MeshPhongMaterial({ color: 0x202020 });

// Physics variables
var gravityConstant = 7.8;
var collisionConfiguration;
var dispatcher;
var broadphase;
var solver;
var physicsWorld;
var margin = 0.05;
var mouse = { x: 0, y: 0 };
var fbx;
var touch = false;
var tilt = false;
var beta = 0;
var gamma = 0;
var canvasMain;
var imageOpen = false;
var factor = 70;
var initial = false;
var shakeX = 0;
var shakeY = 0;
var acc = { x: 0, y: 0, z: 0 };

var bound = [];
var boundShape = [];
// Rigid bodies include all movable objects
var rigidBodies = [];
var objects = [];

var resizeTimeout;
var resizeX;
var resizeY;
var isAndroid = false;

var mouseGrav = { x: 0, y: 0 };
var angleTimeout = false;
var tilt = false;

var tTexture = 0;

var pos = new THREE.Vector3();
var viewWidth = 0;
var quat = new THREE.Quaternion();
var transformAux1 = new Ammo.btTransform();
var tempBtVec3_1 = new Ammo.btVector3(0, 0, 0);

var brick;
var mouse;
var dancelab;
var agenda;
var phone;

var screenD;
var screenT;
var screenM;
var t = 0;

var colliders = [];

var time = 0;

window.addEventListener("load", () => {
  if ("ontouchstart" in document.documentElement) {
    touch = true;
  }

  var ua = navigator.userAgent.toLowerCase();
  isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");

  resizeX = window.innerWidth;
  resizeY = window.innerHeight;
  init();
});

window.addEventListener("resize", resizeHandler);

function resizeHandler() {
  var w = window.innerWidth;
  var h = window.innerHeight * 1;

  if (!touch) {
    clearTimeout(resizeTimeout);
    updateCam();

    renderer.setSize(window.innerWidth, h);

    for (var i = 0; i < bound.length; i++) {
      physicsWorld.removeRigidBody(bound[i]);
    }
    for (var i = 0; i < boundShape.length; i++) {
      scene.remove(bound[i]);
    }

    createObjects();

    resizeX = w;
    resizeY = h;
  }
}

function init() {
  initGraphics();
  initPhysics();
  animate();
  listeners3D();
}

function listeners3D() {
  document.addEventListener("mousemove", onDocumentMouseMove, false);

  window.addEventListener("devicemotion", motion, false);
}

function initGraphics() {
  // RENDERER ********************************

  container = document.getElementById("container");

  // canvasMain = document.getElementById("canvas");
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setClearColor(0xffffff, 0);
  renderer.antialias = true;
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = false;

  container.innerHTML = "";
  container.appendChild(renderer.domElement);

  // CAMERA/SCENE ********************************

  camera = new THREE.OrthographicCamera(
    window.innerWidth / -factor,
    window.innerWidth / factor,
    (window.innerHeight * 1) / factor,
    (window.innerHeight * 1) / -factor,
    -1000,
    1000
  );
  camera.position.set(0, 10, 0);
  camera.rotation.set((3 * Math.PI) / 2, 0, 0);
  scene = new THREE.Scene();

  // LIGHTS ********************************

  var ambient = new THREE.AmbientLight(0xffffff, 0.9);
  scene.add(ambient);
  var directionalLight = new THREE.DirectionalLight(0xffffff, 0.3);
  directionalLight.position.set(100, 350, 250);
  directionalLight.castShadow = true;
  scene.add(directionalLight);

  // TEXTURES ********************************

  var textureLoader = new THREE.TextureLoader();

  var hdri = textureLoader.load("/fbx/test_env2.png");
  hdri.mapping = THREE.SphericalReflectionMapping;

  // LOADING ********************************

  var manager = new THREE.LoadingManager();
  manager.onLoad = function () {
    // document.getElementById("top").style.opacity = "1";
    // document.getElementById("bottom").style.opacity = "1";
    setTimeout(() => {
      container.querySelector("canvas").classList.add("opacity");
    }, 800);
  };

  // FBX ********************************

  var loader = new THREE.FBXLoader(manager);
  loader.load("/fbx/rocks.fbx", function (object) {
    const add = () => {
      fbx = object;

      object.traverse(function (child) {
        if (child.isMesh) {
          // console.log(child);
          let bounding = new THREE.Box3().setFromObject(child);
          colliders[child.name] = [
            bounding.max.x / 10,
            bounding.max.y / 10,
            bounding.max.z / 10,
          ];
          // console.log(colliders);
          child.material = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            reflectivity: 1,
            refractionRatio: 0.8,
            envMap: hdri,
          });
        }
      });
      object.scale.set(0.3, 0.3, 0.3);
      object.rotation.z = 90;
      object.position.set(0, 0, 0);
      createObjects();
    };
    add();
  });
}

function initPhysics() {
  // Physics configuration
  collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
  dispatcher = new Ammo.btCollisionDispatcher(collisionConfiguration);
  broadphase = new Ammo.btDbvtBroadphase();
  solver = new Ammo.btSequentialImpulseConstraintSolver();
  physicsWorld = new Ammo.btDiscreteDynamicsWorld(
    dispatcher,
    broadphase,
    solver,
    collisionConfiguration
  );
  physicsWorld.setGravity(new Ammo.btVector3(0, -gravityConstant, 0));
}

function createObjects() {
  var w = (window.innerWidth + 30) / 35;
  var h = (window.innerHeight * 1 + 30) / 35;

  viewWidth = w;

  // Ground
  pos.set(0, -10, 0);
  quat.set(0, 0, 0, 1);
  var ground = createParalellepipedWithPhysics(
    w,
    1,
    h,
    0,
    pos,
    quat,
    new THREE.MeshPhongMaterial({ color: 0xff0000 })
  );
  ground.visible = false;
  boundShape.push(ground);

  pos.set(0, 20, 0);
  quat.set(0, 0, 0, 1);
  var ground = createParalellepipedWithPhysics(
    w,
    1,
    h,
    0,
    pos,
    quat,
    new THREE.MeshPhongMaterial({ color: 0xffff00 })
  );
  ground.visible = false;
  boundShape.push(ground);

  pos.set(0, 0, h / 2);
  quat.set(0, 0, 0, 1);
  var ground = createParalellepipedWithPhysics(
    w,
    50,
    1,
    0,
    pos,
    quat,
    new THREE.MeshPhongMaterial({ color: 0x0000ff })
  );
  ground.visible = false;
  boundShape.push(ground);

  pos.set(0, 0, -h / 2);
  quat.set(0, 0, 0, 1);
  var ground = createParalellepipedWithPhysics(
    w,
    50,
    1,
    0,
    pos,
    quat,
    new THREE.MeshPhongMaterial({ color: 0x0000ff })
  );
  ground.visible = false;
  boundShape.push(ground);

  pos.set(w / 2, 0, 0);
  quat.set(0, 0, 0, 1);
  var ground = createParalellepipedWithPhysics(
    1,
    50,
    h,
    0,
    pos,
    quat,
    new THREE.MeshPhongMaterial({ color: 0x0000ff })
  );
  ground.visible = false;
  boundShape.push(ground);

  pos.set(-w / 2, 0, 0);
  quat.set(0, 0, 0, 1);
  var ground = createParalellepipedWithPhysics(
    1,
    50,
    h,
    0,
    pos,
    quat,
    new THREE.MeshPhongMaterial({ color: 0x0000ff })
  );
  ground.visible = false;
  boundShape.push(ground);

  // var infoHeight = w/6;
  // if(mobile){
  // 	infoHeight = w/3.6;
  // }
  // var infoWidth = w/1.3;
  // if(mobile){
  // 	infoWidth = w/1.25;
  // }

  if (!initial) {
    makeObjects(0);
    // makeMice(5);
    // makeBrick(2);
    // makePhone(1);
    initial = true;
  }
}

function makeObjects(index) {
  var stoneMass = 130;
  var numStones = 20;
  quat.set(4, 1, 4, 2);

  var ball = fbx.children[index].clone();
  var name = ball.name;
  var c = colliders[name];

  pos.set(0, 0, 1);
  stoneMass = 130 + Math.random() * 100 - 50;

  var obj = createBallsWithPhysics(
    ball,
    c,
    stoneMass,
    pos,
    quat,
    new THREE.MeshStandardMaterial({
      color: 0xffffff,
      roughness: 0,
      metalness: 100,
    }),
    index
  );
  if (index < fbx.children.length - 1) {
    setTimeout(function () {
      makeObjects(index + 1);
    }, 0);
  }
}

function createParalellepipedWithPhysics(
  sx,
  sy,
  sz,
  mass,
  pos,
  quat,
  material
) {
  var object = new THREE.Mesh(
    new THREE.BoxBufferGeometry(sx, sy, sz, 1, 1, 1),
    material
  );
  var shape = new Ammo.btBoxShape(
    new Ammo.btVector3(sx * 0.5, sy * 0.5, sz * 0.5)
  );
  shape.setMargin(margin);

  createRigidBody(object, shape, mass, pos, quat);

  return object;
}

function createBallsWithPhysics(
  mesh,
  collider,
  mass,
  pos,
  quat,
  material,
  index
) {
  var w = window.innerWidth;
  var h = window.innerHeight;

  var ballMass = 35;
  var ballRadius = 1.2;

  if (w < 640) {
    mesh.scale.set(0.04, 0.04, 0.04);
    var ballShape = new Ammo.btBoxShape(
      new Ammo.btVector3(
        collider[0] * 0.4,
        collider[1] * 0.4,
        collider[2] * 0.4
      )
    );
  } else {
    if (w < 1400 && h < 1000) {
      mesh.scale.set(0.075, 0.075, 0.075);
      var ballShape = new Ammo.btBoxShape(
        new Ammo.btVector3(
          collider[0] * 0.75,
          collider[1] * 0.75,
          collider[2] * 0.75
        )
      );
    } else {
      var ex = w - 1400;
      if (ex < 0) {
        ex = 0;
      }
      var scale = 1 + ex / w;
      mesh.scale.set(scale / 10, scale / 10, scale / 10);
      var ballShape = new Ammo.btBoxShape(
        new Ammo.btVector3(
          collider[0] * scale,
          collider[1] * scale,
          collider[2] * scale
        )
      );
    }
  }
  ballShape.setMargin(margin);

  var pos = new THREE.Vector3(0, -10 + 1 * index, 0);
  var ang = new THREE.Quaternion();
  ang.set(5, 5, 5, 5);

  createRigidBody(mesh, ballShape, mass, pos, quat, pos, ang);

  return;
}

function createRigidBody(object, physicsShape, mass, pos, quat, vel, angVel) {
  if (pos) {
    object.position.copy(pos);
  } else {
    pos = object.position;
  }
  if (quat) {
    object.quaternion.copy(quat);
  } else {
    quat = object.quaternion;
  }

  var transform = new Ammo.btTransform();
  transform.setIdentity();
  transform.setOrigin(new Ammo.btVector3(pos.x, pos.y, pos.z));
  transform.setRotation(new Ammo.btQuaternion(quat.x, quat.y, quat.z, quat.w));
  var motionState = new Ammo.btDefaultMotionState(transform);

  var localInertia = new Ammo.btVector3(0, 0, 0);
  physicsShape.calculateLocalInertia(mass, localInertia);

  var rbInfo = new Ammo.btRigidBodyConstructionInfo(
    mass,
    motionState,
    physicsShape,
    localInertia
  );
  var body = new Ammo.btRigidBody(rbInfo);

  body.setFriction(0);

  if (vel) {
    body.setLinearVelocity(new Ammo.btVector3(vel.x, vel.y, vel.z));
  }
  if (angVel) {
    body.setAngularVelocity(new Ammo.btVector3(angVel.x, angVel.y, angVel.z));
  }

  object.userData.physicsBody = body;
  object.userData.collided = false;

  scene.add(object);

  if (mass > 0) {
    rigidBodies.push(object);
    objects.push(body);

    // Disable deactivation
    body.setActivationState(4);
  } else {
    bound.push(body);
  }

  physicsWorld.addRigidBody(body);

  return body;
}

function animate() {
  requestAnimationFrame(animate);
  render();
}

function render() {
  var deltaTime = clock.getDelta();

  updatePhysics(deltaTime);

  renderer.render(scene, camera);

  time += deltaTime;
}

function updateCam() {
  var w = window.innerWidth;
  var h = window.innerHeight;
  camera.left = -w / factor;
  camera.right = w / factor;
  camera.top = h / factor;
  camera.bottom = -h / factor;
  camera.updateProjectionMatrix();
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

function onDocumentMouseMove(event) {
  event.preventDefault();
  mouse.x = event.clientX - window.innerWidth / 2;
  mouse.y = event.clientY - window.innerHeight / 2;
  mouseGrav.x = event.clientX / window.innerWidth / 2;
  mouseGrav.y = -(event.clientY / window.innerHeight) / 2;
}

function motion(event) {
  acc.x = event.accelerationIncludingGravity.x;
  acc.y = event.accelerationIncludingGravity.y;
  acc.z = event.accelerationIncludingGravity.z;
}

function updatePhysics(deltaTime) {
  // Step world
  physicsWorld.stepSimulation(deltaTime, 10);
  if (touch && tilt) {
    var gravFactor = 3;
    if (!angleTimeout) {
      for (var i = 0; i < objects.length; i++) {
        objects[i].setAngularVelocity(new Ammo.btVector3(0.1, 0.1, 0.1));
      }
    }
  } else {
    var gravFactor = 50;
    var gravX = mouse.x / (window.innerWidth / 2);
    var gravY = mouse.y / (window.innerHeight / 2);

    if (gravX <= -0.5) {
      gravX = (gravX + 0.5) * gravFactor;
    } else {
      if (gravX >= 0.5) {
        gravX = (gravX - 0.5) * gravFactor;
      } else {
        gravX = 0;
      }
    }

    if (gravY <= -0.5) {
      gravY = (gravY + 0.5) * gravFactor;
    } else {
      if (gravY >= 0.5) {
        gravY = (gravY - 0.5) * gravFactor;
      } else {
        gravY = 0;
      }
    }

    physicsWorld.setGravity(new Ammo.btVector3(gravX, 0, gravY));

    if (gravX == 0 && gravY == 0 && !angleTimeout) {
      for (var i = 0; i < objects.length; i++) {
        objects[i].setAngularVelocity(new Ammo.btVector3(0.1, 0.1, 0.1));
      }
    }
  }

  // Update rigid bodies
  for (var i = 0, il = rigidBodies.length; i < il; i++) {
    var objThree = rigidBodies[i];
    var objPhys = objThree.userData.physicsBody;
    var ms = objPhys.getMotionState();
    if (ms) {
      ms.getWorldTransform(transformAux1);
      var p = transformAux1.getOrigin();
      var q = transformAux1.getRotation();
      objThree.position.set(p.x(), p.y(), p.z());
      objThree.quaternion.set(q.x(), q.y(), q.z(), q.w());

      objThree.userData.collided = false;
    }
  }
}
