// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const { GoogleSpreadsheet } = require("google-spreadsheet");

export default async (req, res) => {
  const SPREADSHEET_ID = process.env.SPREADSHEET_ID;
  const PRIVATE_KEY = process.env.PRIVATE_KEY;
  const CLIENT_EMAIL = process.env.CLIENT_EMAIL;

  const doc = new GoogleSpreadsheet(SPREADSHEET_ID);
  doc.useServiceAccountAuth({
    client_email: CLIENT_EMAIL,
    private_key: PRIVATE_KEY,
  });

  console.log(req.body);
  await doc.loadInfo();
  const sheet = doc.sheetsById["0"];
  const result = await sheet.addRow(req.body);
  res.status(200).json({ result });
};
