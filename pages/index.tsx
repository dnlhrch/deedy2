import Head from "next/head";
import React from "react";
import styled from "styled-components";
import axios from "axios";
import { motion, useAnimation } from "framer-motion";
import { GoogleSpreadsheet } from "google-spreadsheet";
import Ticker from "react-ticker";
import Marquee from "react-fast-marquee";
import _ from "lodash";

export default function Home() {
  const [data, setData] = React.useState({
    email: "",
  });
  const [state, setState] = React.useState<"initial" | "form" | "submitted">(
    "initial"
  );
  const [isMobile, setIsMobile] = React.useState(false);
  const inputController = useAnimation();
  const topWrapperController = useAnimation();
  const otherController = useAnimation();
  const [button, setButton] = React.useState(false);

  const logoRef = React.useRef<null | HTMLDivElement>(null);
  const inputRef = React.useRef<null | HTMLInputElement>(null);
  const boundRef = React.useRef<null | HTMLDivElement>(null);

  const changeFontWidthOnMouse = (e: MouseEvent) => {
    if (state !== "form" && window.innerWidth > 768) {
      let multiplierWidth = e.clientX / window.innerWidth;
      let width = multiplierWidth * 400 + 250;
      //@ts-ignore
      logoRef.current.style.fontVariationSettings = '"wdth" ' + width;
    }
  };

  React.useEffect(() => {
    setIsMobile(window.innerWidth < 768);
    window.addEventListener("resize", () => {
      setIsMobile(window.innerWidth < 768);
    });
    window.addEventListener("mousemove", changeFontWidthOnMouse);
    topWrapperController.start("show");
    otherController.start("show");
  }, []);

  const emailCheck = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const onClick = async () => {
    switch (state) {
      case "initial":
        setState("form");
        window.removeEventListener("mousemove", changeFontWidthOnMouse);
        setTimeout(() => inputRef.current.focus(), 500);
        break;
      case "form":
        // console.log();
        if (emailCheck.test(data.email)) {
          setState("submitted");
          const SPREADSHEET_ID = "16Oq_lV-Pwtwm19XMR1kES1HwP3Ro0R8pGetvrCVrowA";
          const PRIVATE_KEY =
            "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDDd9Juei5S7Hpr\nnRnLfl/EdwWEySZuek7fi6uKsM0M352oWEguRDoo+QDePSbGNQhmPWrx0AyJXli4\nFWrfq8NaAJCtdOvKYPkxYtCRvN1y30+sXeWiEodfsOZ2e4SF7F4XJ6Tz2x2QzHbI\njsmPEKGZpbD5zP9v8gSmUaVLNLfpA/dm0pfeGtQjfb8vmMri2oAfZhsLC+Cu4x1T\nQy2IRM2A9NhqUzRclkUfohvyNIHhsEU5RBeclzqrdPsEAwE0bpfgf2+5bbrYaaJU\n8pW/00y4ImrgxrTasCR3Bc40+b2kCM+hKloxbZDVjiMGMcxPj51Ep23B1/vF10i0\n/dJMs0fBAgMBAAECggEABYlQCkWHKN2eVQx41dDxRPcLDrkEz+XCdKX32KCL2kS+\nKi2/86GGlTECU4kfX0dSwtSUAh/erwhfUm123rl2ecbg0izo3c1T2ZB/MFWNXyrQ\njqUFM0s7jtwLlwkGFqX8f0Ll8us+oeKMrb8PmfACZ7/v7I9gH8tW0uAmoqPuXpjr\nvYUhXSvbV9uYuW8I6bY8Rd33l40FrD9VoQhX2xOwPiqhjRvOMgBn//KKD+5mrLcj\n5M77bZSC33QYBPan9AnqbXm1QEpKJ0p5TKZmA8ctSMl3R4ymmoko+5Q5kZPv1RQO\naQYxS3nfUcMJj9CxCjqBMTuVFGmR7AcIQBjSPu5VzQKBgQDiGytxDKUU3x+1F1OD\nr/gQ0TF4vWx06xqZMWWorjrRD1w2Cuz1blUxY/URa/jNXwLnoufKWaHK3QRxubxZ\n9AN/azDWybQMTbV3UnE1wcFOUwXiQ9kRHtGmMjh6fPbj5AbbSERTBIRPEQOMnYyn\nAKMVKn5i87Tki4UWaHQOJeXtbQKBgQDdT6uUsV1nUXoyXG698R1BrUNcntZh+GVW\n8ya7PBay7ALL/Tm6JcjRNeU9NhOgf0FC+jEaQoTWc97EPWQ2aZGcuzW97oNRTUfc\nG1Y4Wk6qpxFsKpvEdlv9s402O5QwLz/YIhBtNRgJlZoXsAOrk7xyEomJr3cjAYHp\nDLWTOhBzJQKBgGICVPpT+NrrPNtKmTDj/iuMAl3L/k9P+d+dwes0Wtp0RgxHACAD\nC3q1UW2T1+1t2dx+iOr1n2RIGb0SPcrgMwPweuMf26PZq2dlnR0oN5qz8kD6GAWL\nzmS0MDcoI/z0wuQal0NFGfUL8vXo9ScO56ItIUvTR/4pXC8yZ9waIWihAoGBAKV4\nkTi/GLBMeko3VFHOeuqFSs1wsahU2iiV6wqples3javJ7SSiZbEPVBRdX3VsUHZs\nrNQOklXqEtC1ej0417fRPhCByku6tZT8F45Q72qnOd6fSRIu0P8zBc1b5KPWYC0Q\n4ylUPmHEKo+1ejkKYCcGdzGsMS7KTMDcUNMA4hXFAoGBALoitETZ2ag7Es2HzxDn\n2vtsG7Dt/HmyHi2V6m4IgZKCa5rssTgVaJAbMhzfvWvWfgNCNyQRwjKjlk7c9327\niGso+lzGNcLac5nTMewIqbLQjWoXzeaa7JXRuNzLhEbzeyJMIDCFTjfxw0Y4QCFs\nGzrdAA0Cr1v7GbhNzl7MJyFY\n-----END PRIVATE KEY-----\n";
          const CLIENT_EMAIL =
            "deedy-993@seventh-fact-310119.iam.gserviceaccount.com";

          const doc = new GoogleSpreadsheet(SPREADSHEET_ID);
          doc.useServiceAccountAuth({
            client_email: CLIENT_EMAIL,
            private_key: PRIVATE_KEY,
          });

          await doc.loadInfo();
          const sheet = doc.sheetsById["0"];
          const result = await sheet.addRow(data);
        }
        break;
    }
  };

  const deriveButtonLabel = () => {
    switch (state) {
      case "initial":
        return "Sign up";
      case "form":
        return "Send";
      case "submitted":
        return "Thank you!";
    }
  };

  return (
    <div>
      <Head>
        <title>Deedy</title>
      </Head>
      <ContentWrapper id="content">
        <TopWrapper
          id="topWrapper"
          variants={{
            initial: { opacity: 0 },
            show: {
              opacity: 1,
              transition: {
                ease: [0.62, 0.04, 0.33, 1],
                duration: 1,
                delay: 2,
              },
            },
          }}
          initial={"initial"}
          animate={topWrapperController}
        >
          <TopText>
            NFT Marketplace<br></br>
            <br></br>Create.<br></br>Connect.<br></br>Collaborate.
          </TopText>
          <SocialWrapper>
            <SocialButtonWrapper
              target="_blank"
              href="https://mobile.twitter.com/DigitalDeedy"
            >
              <SocialButton src="/twitter.svg" />
            </SocialButtonWrapper>
            <SocialButtonWrapper
              target="_blank"
              href="https://t.me/deedydigitalchannel"
            >
              <SocialButton src="/telegram.svg" />
            </SocialButtonWrapper>
          </SocialWrapper>
        </TopWrapper>
        <LogoWrapper
          id="logoWrapper"
          variants={{
            initial: { opacity: 0 },
            show: {
              opacity: 1,
              transition: {
                ease: [0.62, 0.04, 0.33, 1],
                duration: 1,
              },
            },
          }}
          initial={"initial"}
          animate={"show"}
        >
          <Logo display={state === "form"} ref={logoRef}>
            deedy
          </Logo>
          {state === "form" && (
            <Input
              id="input"
              ref={inputRef}
              onChange={(e) => {
                setData({ email: e.target.value });
              }}
              placeholder="Your email"
            />
          )}
          <Text
            variants={{
              initial: { opacity: 0 },
              show: {
                opacity: 1,
                transition: {
                  ease: [0.62, 0.04, 0.33, 1],
                  duration: 1,
                },
              },
            }}
            initial={"initial"}
            animate={"show"}
          >
            Welcome to the future of ownership
          </Text>
        </LogoWrapper>
        <ButtonBlock>
          <ButtonWrapper
            variants={{
              initial: { opacity: 0 },
              hover: { borderRadius: "50%" },
              show: {
                opacity: 1,
                transition: {
                  ease: [0.62, 0.04, 0.33, 1],
                  duration: 1,
                  delay: 2,
                },
              },
            }}
            initial={"initial"}
            animate={"show"}
          >
            <Button
              id="button"
              onClick={onClick}
              onMouseEnter={() => setButton(true)}
              onMouseLeave={() => setButton(false)}
            >
              {(button && !isMobile) || (isMobile && state === "form") ? (
                <Ticker>
                  {() => <>&nbsp;&nbsp;{deriveButtonLabel()}&nbsp;&nbsp;</>}
                </Ticker>
              ) : (
                deriveButtonLabel()
              )}
            </Button>
          </ButtonWrapper>
        </ButtonBlock>
        <TickerWrapper
          id="tickerWrapper"
          variants={{
            initial: { opacity: 0 },
            show: {
              opacity: 1,
              transition: {
                ease: [0.62, 0.04, 0.33, 1],
                duration: 1,
                delay: 2,
              },
            },
          }}
          initial={"initial"}
          animate={"show"}
        >
          <Marquee gradient={false} speed={50}>
            <p>
              &nbsp;&nbsp;Join our early access list to be the first to receive
              updates&nbsp;&nbsp;
            </p>
            <p>
              &nbsp;&nbsp;Join our early access list to be the first to receive
              updates&nbsp;&nbsp;
            </p>
            <p>
              &nbsp;&nbsp;Join our early access list to be the first to receive
              updates&nbsp;&nbsp;
            </p>
            <p>
              &nbsp;&nbsp;Join our early access list to be the first to receive
              updates&nbsp;&nbsp;
            </p>
          </Marquee>
        </TickerWrapper>
      </ContentWrapper>
      <CanvasWrapper id="container" />
    </div>
  );
}

const CanvasWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  pointer-events: none;
  filter: invert(1);
  z-index: 1;
`;

const ContentWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  z-index: 2;
`;

const TopWrapper = styled(motion.div)`
  width: 96vw;
  margin: 2vw auto;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  @media screen and (max-width: 768px) {
    width: 90vw;
    margin: 6vw auto;
  }
`;

const TopText = styled.p`
  font-family: "SharpGrotesk";
  font-size: 1.25vw;
  color: white;
  text-align: left;
  @media screen and (max-width: 768px) {
    font-size: 4.8vw;
  }
`;

const SocialWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  @media screen and (max-width: 768px) {
  }
`;

const SocialButtonWrapper = styled.a`
  margin-left: 1vw;
  @media screen and (max-width: 768px) {
  }
`;

const SocialButton = styled.img`
  width: 1.25vw;
  height: 1.25vw;
  object-fit: cover;
  object-position: center;
  @media screen and (max-width: 768px) {
    width: 5.74vw;
    height: 5.74vw;
  }
`;

const LogoWrapper = styled(motion.div)`
  width: 98vw;
  top: 50%;
  transform: translate(-50%, -50%);
  position: absolute;
  left: 50%;
`;

const Logo = styled.div<{ display: boolean }>`
  font-family: "Fit";
  font-size: 12.5vw;
  object-fit: contain;
  object-position: center;
  line-height: 1;
  transition: opacity 0.5s ease;
  display: ${({ display }) => (display ? "none" : "block")};
  @media screen and (max-width: 768px) {
    font-size: 20vw;
    font-variation-settings: "wdth" 350;
  }
`;

const Text = styled(motion.div)`
  font-size: 1.25vw;
  @media screen and (max-width: 768px) {
    font-size: 3.5vw;
  }
`;

const FormInnerWrapper = styled(motion.div)`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const Input = styled.input`
  font-family: "Fit";
  border: none;
  outline: none;
  width: 100vw;
  text-align: center;
  font-size: 12.5vw;
  width: 100vw;
  background-color: rgba(0, 0, 0, 0);
  color: white;
  font-variation-settings: "wdth" 100;
  &::placeholder {
    color: rgba(255, 255, 255, 0.4);
  }
  box-sizing: border-box;
  @media screen and (max-width: 768px) {
    border: 0;
    width: 100vw;
    font-size: 20vw;
    background-color: rgba(0, 0, 0, 0);
    color: white;
    font-variation-settings: "wdth" 200;
    &::placeholder {
      color: white;
    }
    box-sizing: border-box;
  }
`;

const ButtonBlock = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(50vh - ((14vw / 2) + 5.5vw));
  width: 100vw;
  position: absolute;
  bottom: 5.5vw;
`;

const Button = styled.button`
  font-family: "SharpGrotesk";
  text-transform: uppercase;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border: 0.2vw solid white;
  padding: 0;
  outline: none;
  width: 11vw;
  height: 3.3333333333vw;
  border-radius: 3.3333333333vw;
  background-color: rgba(0, 0, 0, 0);
  display: flex;
  justify-content: center;
  overflow: hidden;
  transition: all 0.15s linear;
  cursor: pointer;
  align-items: center;
  color: white;
  font-size: 1.25vw;
  @media screen and (max-width: 768px) {
    width: 32.6666666667vw;
    height: 9.3333333333vw;
    border-radius: 9.3333333333vw;
    background-color: rgba(0, 0, 0, 0);
    border: 0.7vw solid white;
    display: flex;
    justify-content: center;
    align-items: center;
    color: white;
    font-size: 4vw;
  }
`;

const TickerWrapper = styled(motion.div)`
  position: absolute !important;
  margin: 2vw 0;
  bottom: 0;
  width: 100%;
  p {
    width: 52vw;
    @media screen and (max-width: 768px) {
      width: 170vw;
      font-size: 4vw;
    }
  }
`;

const ButtonWrapper = styled(motion.div)`
  opacity: 1;
  width: 100vw;
  position: absolute;
`;
