import "reset-css";
import "../styles/globals.css";

import Head from "next/head";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <script type="module" src="/3d/jquery.min.js"></script>
        <script src="/3d/three.min.js"></script>
        <script src="/3d/FBXLoader.js"></script>
        <script
          type="module"
          src="https://threejs.org/examples/jsm/postprocessing/UnrealBloomPass.js"
        ></script>
        <script
          type="module"
          src="https://threejs.org/examples/jsm/postprocessing/RenderPass.js"
        ></script>
        <script src="/3d/inflate.min.js"></script>
        <script src="/3d/ammo.js"></script>
        <script src="/3d/index.js"></script>
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
